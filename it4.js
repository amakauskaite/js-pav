//form validation
//field cannot be empty

function fieldIsNotEmpty() {
    //var name = document.forms["newSubject"]["subjectName"].value;
    if (document.forms['newSubject']['subjectName'].value == "") {
        alert("Trūksta dalyko pavadinimo");
        return false;
    }
    else return true;
}

function numberIsIntegerAndPositive() {
    var score = document.forms['newSubject']['score'].value;
    //var score = arguments[0];

    if (score != "") {
        if (isNaN(score)) {
            alert("Pažymys turi būti skaičius");
            return false;
        }
        if (score < 0) {
            alert("Pažymys turi būti teigiamas");
            return false;
        }
        if (parseInt(score) != score) {
            alert("Pažymys turi būti sveikas skaičius");
            return false;
        }
    }

    return true;

}

function styleContent() {

    //$(".examDate2").hide();
    $(".examDate2").css("display", "none");
    $(".congrats").hide();
    $("table, td, tr, th").attr("style", "border : 1px solid");
}

function showDate2Field() {
    var dateField = document.forms['newSubject']['score'].value;

     if (dateField >= 5) {
        $(".examDate2").css("display", "none");
        $("#congrats").text("Sveikinu!");
        $(".congrats").show();
        $("#congrats").attr("style", "color : blue");
    }
    else if (dateField<5 && dateField > 0){
        $(".examDate2").css("display", "initial");
        $("#congrats").text("Uzjauciu :/");
        $("#congrats").attr("style", "color : purple");
        $(".congrats").show();
        //$(".examDate2").show();
        //$(".examDate2").style.display = 'initial';
    } else {
         $(".examDate2").css("display", "none");
         $("#congrats").text("");
         $(".congrats").hide();
         //$(".examDate2").hide();
         //$(".examDate2").style.display = 'none';
     }
}

function addItemToTable() {
    if(fieldIsNotEmpty() === true && numberIsIntegerAndPositive() === true)
    {
        $("#scoreTable").append("<tr>\n" +
            "            <td>" + $("#subjectName").val() + "</td>\n" +
            "            <td>" + $("#score").val() + "</td>\n" +
            "            <td>" + $("#examDate").val() + "</td>\n" +
            "            <td>" + repeatExamDate() + "</td>\n" +
            "        </tr>");

        deleteExam();

        $("#subjectName").val("");
        $("#score").val("");
        $("#examDate").val("");
        $("#examDate2field").val("");
    }
    else alert("Neteisingai užpildyta forma!");

}

function repeatExamDate() {
    if ($("#examDate2field").val() == "")
        return "nėra";
    else return $("#examDate2field").val();
}

function deleteExam() {

    $("li").each(function () {
        //$(this).text()
        if ($(this).text() === $("#subjectName").val()) {
            $(this).remove();
        }
    });
    //$("//ul > li").last().remove();
}

function getMyJSON() {

    var request = $.get("https://api.myjson.com/bins/7572z", function (data, textStatus, jqXHR) {
        $.each(data, function (index) {
            content = "<tr>\n" +
                "            <td>" + data[index].subjectName + "</td>\n" +
                "            <td>" + data[index].score + "</td>\n" +
                "            <td>" + data[index].examDate + "</td>\n" +
                "            <td>" + data[index].repeatDate + "</td>\n" +
                "        </tr>";

            $("#scoreTable").append(content);
            //$(content).appendTo("#scoreTable");
        })
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Getting failed: " + textStatus);
    });

}

